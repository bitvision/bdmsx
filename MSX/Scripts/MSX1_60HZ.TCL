set fullscreen false
# si está a 50Hz
if {[expr [vdpreg 9] & 2] == 2 } {		
	# cambia la frecuencia a 60Hz
	after 3000 toggle_freq													
}
